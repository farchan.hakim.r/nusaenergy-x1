var express = require('express');
var router = express.Router();

const relayController = require('../controller/relayControoler');
const auth = require('./../middleware/authentication')

//buat kirim parameter
router.post('/set/:building_name/:hardware_type', auth.verifyToken, relayController.setRelay, function(req, res, next) {});

router.get('/set/:building_name/:hardware_type', auth.verifyToken, relayController.getStatus, function(req, res, next) {

});


module.exports = router;