var express = require('express');
var router = express.Router();

const userController = require('./../controller/staffController');
const auth = require('./../middleware/authentication')
/* GET users listing. */
router.get('/', auth.verifyToken, function(req, res) {
  res.send('respond with a resource');
});

router.post('/login', userController.getLogin, function(req,res,next) {} );

router.post('/register', userController.staffRegister, function(req,res,next) {} );

module.exports = router;

