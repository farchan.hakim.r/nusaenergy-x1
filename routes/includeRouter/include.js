//okokok123@
const express = require('express');
const logger = require('morgan');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
const { autoIncrement } = require('mongoose-plugin-autoinc');
const moment2 = require('moment');
var moment = require('moment-timezone');
moment().tz("Asia/Jakarta").format();
const float = require('mongoose-float').loadType(mongoose);
const cors = require('cors');
var stringify = require('json-stable-stringify');
const _ = require('underscore');

mongoose.connect('mongodb://localhost/nusaenergy', { //'mongodb+srv://tamtech_dev:GiwP36HsujCMRr6L@faizal-ssde5.gcp.mongodb.net/test?retryWrites=true'
    useCreateIndex: true,
    useNewUrlParser: true
});

const app = express();

exports.express = express;
exports.app = app;
exports.logger = logger;
exports.jwt = jwt;
exports.path = path;
exports.cookieParser = cookieParser;
exports.bodyParser = bodyParser;
exports.bcrypt = bcrypt;
exports.mongoose = mongoose;
exports.beautifyUnique = beautifyUnique;
exports.autoIncrement = autoIncrement;
exports.moment = moment;
exports.float = float;
exports.cors = cors;
exports.stringify = stringify;
exports._ = _;