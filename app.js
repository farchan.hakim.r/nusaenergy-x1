const include = require('./routes/includeRouter/include');

const express = include.express;
const cors = include.cors;

const app = express();
app.use(cors())
app.disable('etag');

const logger = include.logger;
const path = include.path;
const bodyParser = include.bodyParser;

const user = require('./routes/users');
const sensor = require('./routes/sensorRouter');
const relay = require('./routes/relayRouter');



app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/sensor', sensor);

app.use('/relay', relay);

app.use('/user', user);



module.exports = app;