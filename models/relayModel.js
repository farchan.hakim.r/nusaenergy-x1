const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const relaySchema = new mongoose.Schema({
    building_name: { type: String },
    hardware_type: { type: String },
    detail: [{
        _id: false,
        relay_status: {
            by_hardware: { type: Boolean },
            by_software: { type: Boolean }
        },
        kwh: { type: Number },
        volt: { type: Number },
        current: { type: Number },
        created_at: { type: String, default: moment().format() },
    }],
})

const RelaySchema = mongoose.model('relayModel', relaySchema);

module.exports = RelaySchema;