const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const staffSchema = new mongoose.Schema({
    nip: { type: String, unique: true, required: true },
    building_name: { type: String },
    detail: [{
        name: { type: String },
        pass: { type: String },
        role: { type: Number },
        created_at: { type: String, default: moment().format() },
    }],
})

const Staff = mongoose.model('Staff', staffSchema);

module.exports = Staff;