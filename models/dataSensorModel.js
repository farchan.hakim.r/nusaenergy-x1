const include = require('../routes/includeRouter/include');

const mongoose = include.mongoose;
const moment = include.moment;

const sensorSchema = new mongoose.Schema({
    building_name: { type: String },
    hardware_type: { type: String },
    status: { type: Boolean, default: true },
    detail: [{
        _id: false,
        kwh: { type: Number },
        volt: { type: Number },
        current: { type: Number },
        created_at: { type: String, default: moment().format() },
    }],
})

const SensorSchema = mongoose.model('SensorKwh', sensorSchema);

module.exports = SensorSchema;