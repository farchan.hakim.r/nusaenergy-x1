const include = require('../routes/includeRouter/include');

const moment = include.moment;
const stringify = include.stringify;

const RelayModel = require('../models/relayModel');

const startDay = moment().startOf('day').format();
const endDay = moment().endOf('day').format();


function setRelay(req, res, next) {
    let data = req.body;

    data.building_name = req.params.building_name;
    data.hardware_type = req.params.hardware_type;
    let relaySave = new RelayModel(data);
    let errData = relaySave.validateSync();

    if (res.statusCode == 200) {
        if (!errData) {

            RelayModel.find({ building_name: req.params.building_name, hardware_type: req.params.hardware_type }, function(err, kwhFinds) {
                data.detail[0].created_at = moment().format();
                if (kwhFinds.length < 1) {

                    let relaySaveFix = new RelayModel(data);
                    relaySaveFix.save(function(err, result) {
                        if (err) {
                            res.status(400).json({
                                pesan: "Gagal mengisi",
                            });
                        } else {
                            res.status(200).json({
                                pesan: "Berhasil mengisi data",
                                data: data,
                            });
                        }
                    });
                } else {
                    RelayModel.findOneAndUpdate({ building_name: req.params.building_name, hardware_type: req.params.hardware_type }, {
                            $push: {
                                'detail': {
                                    $each: data.detail,
                                    $position: 0,
                                    $slice: 1000
                                }
                            }
                        },
                        function(err, patient) {
                            res.status(200).json({
                                pesan: "tambah data ",
                                data: data
                            });
                            console.log(JSON.stringify(data))
                        });
                }
            });
        } else {
            res.status(404).json({
                pesan: "data error",
            });
        }
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }

}

function getStatus(req, res, next) {
    if (res.statusCode == 200) {
        RelayModel.find({ building_name: req.params.building_name, hardware_type: req.params.hardware_type }, function(err, kwhFinds) {
            if (kwhFinds.length < 1) {

                res.status(404).json({
                    pesan: "data tidak ditemukan"
                })
            } else {
                res.status(200).json({
                    pesan: "data ditemukan",
                    data: kwhFinds[0].detail[0].relay_status
                })
            }
        });
    } else {
        res.status(404).json({
            pesan: "token tidak berlaku",
        });
    }


}
exports.setRelay = setRelay;
exports.getStatus = getStatus;