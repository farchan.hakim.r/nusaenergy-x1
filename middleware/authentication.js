const include = require('./../routes/includeRouter/include');

const Staff = require('./../models/staffModel');

const config = require('../config');

const jwt = include.jwt;

function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if(typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];

    jwt.verify(bearerToken, config.secret, (err, authData) => {
    	if(err) {
        res.status(401);
      } else {
        res.status(200);
      }
    });
    
  } else {
    res.status(401);
  }
  next();
}

exports.verifyToken = verifyToken;
